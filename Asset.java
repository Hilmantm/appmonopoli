public class Asset {

  // public Komplek komplek;
  // public Stasiun stasiun;
  // public Perusahaan perusahaan;
  public String nama;
  public int hargaSewa;
  public int hargaHipotik;
  // public Uang uang;

  // method untuk komplek
  public void beli() {
    System.out.println("Beli Asset");
  }

  public void jual() {
    System.out.println("Jual Asset");
  }

  public void hipotik() {
    System.out.println("Hipotik Asset");
  }

}
