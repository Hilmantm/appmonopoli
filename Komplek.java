class Komplek extends Asset {

  // Attribut
  public Boolean statusTanah;
  public Boolean statusRumah;
  public Boolean statusHotel;
  // public String namaKomplek;
  public char indexKomplek;
  public int hanyaTanah;
  public int satuRumah;
  public int duaRumah;
  public int tigaRumah;
  public int empatRumah;
  public int satuHotel;

  // statement control
  if( pemain memiliki 1 komplek ) {
    this.satuRumah * 2;
    this.duaRumah * 2;
    this.tigaRumah * 2;
    this.empatRumah * 2;
    this.hanyaTanah * 2;
    this.satuHotel * 2;
  }

  if( pemain sudah melalui 1 putaran ) {
    statusTanah=true;
    pemain bisa membeli tanah
  }

  if( pemain sudah melalui 2 putaran ) {
    statusRumah=true;
    pemain bisa membeli rumah
  }

  if( pemain sudah memiliki 4 rumah ) {
    statusHotel=true;
    pemain bisa membeli hotel
  }

}
